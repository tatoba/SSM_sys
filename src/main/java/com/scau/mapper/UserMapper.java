package com.scau.mapper;

import com.scau.entity.User;
import tk.mybatis.mapper.common.Mapper;


public interface UserMapper extends Mapper<User>{

}