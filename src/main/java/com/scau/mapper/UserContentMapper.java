package com.scau.mapper;

import com.scau.entity.UserContent;
import tk.mybatis.mapper.common.Mapper;


import java.util.List;

public interface UserContentMapper extends Mapper<UserContent> {

}
