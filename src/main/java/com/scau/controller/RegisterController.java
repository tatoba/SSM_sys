package com.scau.controller;
import com.scau.entity.User;
import com.scau.service.UserService;
import com.scau.util.CodeCaptchaServlet;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class RegisterController {
    private final static Logger log = Logger.getLogger(RegisterController.class);

    @Autowired
    private UserService userService;

    @RequestMapping("/checkPhone")
    public ModelAndView checkPhone(@RequestParam(value = "phone",required = false) String phone){
        log.info("注册判断手机号："+phone);
        ModelAndView mv = new ModelAndView();
        User user = userService.findByPhone(phone);
        if(user==null){
            mv.addObject("message","success");
        }else{
            mv.addObject("message","fail");
        }
        return mv;
    }

    @RequestMapping("/checkCode")
    public ModelAndView checkCode(@RequestParam(value = "code",required = false) String code) {
        log.debug("注册判断验证码：" + code);
        ModelAndView mv = new ModelAndView();
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String vcode = (String) attrs.getRequest().getSession().getAttribute(CodeCaptchaServlet.VERCODE_KEY);
        if (code.equals(vcode)) {
            mv.addObject("message", "success");
        }else {
            mv.addObject("message", "fail");
        }
        return mv;

    }

//    @RequestMapping("/checkPhone")
//    @ResponseBody
//    public Map<String, Object> checkPhone(Model model, @RequestParam(value = "phone", required = false) String phone) {
//        log.debug("注册-判断手机号" + phone + "是否可用");
//        Map map = new HashMap<String, Object>();
//        User user = userService.findByPhone(phone);
//        if (user == null) {
//            //未注册
//            map.put("message", "success");
//        } else {
//            //已注册
//            map.put("message", "fail");
//        }
//
//        return map;
//    }


    @RequestMapping("/register")
    public ModelAndView register() {

        log.info("进入注册页面");
        ModelAndView mv = new ModelAndView();
        mv.setViewName("register");
        return mv;


    }
}