package com.scau.controller;

import com.scau.mapper.UserMapper;
import com.scau.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("/user")
public class UserController {
     @Autowired
     private UserService userService;
     @RequestMapping("/tologin")
     public void tologin(){
          System.out.println("成功了");
     }

     @RequestMapping("/login")
     public void login(@RequestParam("name") String name,@RequestParam("age") String age,@RequestParam("id") Long id){
          System.out.println(name);
          System.out.println(userService.findById(id));
     }
}
