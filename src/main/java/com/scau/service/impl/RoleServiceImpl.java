package com.scau.service.impl;

import com.scau.entity.Role;
import com.scau.mapper.RoleMapper;
import com.scau.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    public Role findById(long id) {
        Role role = new Role();
        role.setId( id );
        return roleMapper.selectOne(role);
    }


    public int add(Role role) {
        return roleMapper.insert(role);
    }
}
